<?php

/**
 * @file
 * Install, uninstall, and schema functions for Taxonomy term changed.
 */

/**
 * Implements hook_schema().
 */
function taxonomy_term_changed_schema() {
  $schema['taxonomy_term_history'] = array(
    'description' => 'A record of which {users} have seen which taxonomy terms.',
    'fields' => array(
      'uid' => array(
        'description' => 'The {users}.uid that saw the taxonomy term tid.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'tid' => array(
        'description' => 'The taxonomy term tid that was seen.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'timestamp' => array(
        'description' => 'The Unix timestamp at which the observance occurred.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'primary key' => array('uid', 'tid'),
    'indexes' => array(
      'tid' => array('tid'),
    ),
  );
  return $schema;
}

/**
 * Implements hook_schema_alter().
 */
function taxonomy_term_changed_schema_alter(&$schema) {
  $schema['taxonomy_term_data']['fields']['changed'] = array(
    'description' => 'The Unix timestamp when the term was most recently saved.',
    'type' => 'int',
    'not null' => TRUE,
    'default' => 0,
  );
}

/**
 * Implements hook_install().
 */
function taxonomy_term_changed_install() {
  if (!db_field_exists('taxonomy_term_data', 'changed')) {
    // Add the "changed" column to {taxonomy_term_data}.
    db_add_field('taxonomy_term_data', 'changed', array(
      'description' => 'The Unix timestamp when the term was most recently saved.',
      'type' => 'int',
      'not null' => TRUE,
      'default' => 0,
    ));
  }
}

/**
 * Implements hook_uninstall().
 */
function taxonomy_term_changed_uninstall() {
  db_drop_field('taxonomy_term_data', 'changed');
}
