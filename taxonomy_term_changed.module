<?php

/**
 * @file
 * Tack changes to taxonomy terms.
 */

/**
 * Taxonomy terms changed before this time are always marked as read.
 *
 * Taxonomy terms changed after this time may be marked new, updated, or read,
 * depending on their state for the current user. Defaults to 30 days ago.
 */
define('TAXONOMY_TERM_NEW_LIMIT', REQUEST_TIME - 30 * 24 * 60 * 60);

/**
 * Implements hook_cron().
 */
function taxonomy_term_changed_cron() {
  db_delete('taxonomy_term_history')
    ->condition('timestamp', TAXONOMY_TERM_NEW_LIMIT, '<')
    ->execute();
}

/**
 * Updates the 'last viewed' timestamp of the specified term for current user.
 *
 * @param $term
 *   A taxonomy term object.
 */
function taxonomy_term_tag_new($term) {
  global $user;
  if ($user->uid) {
    db_merge('taxonomy_term_history')
      ->key(array(
        'uid' => $user->uid,
        'tid' => $term->tid,
      ))
      ->fields(array('timestamp' => REQUEST_TIME))
      ->execute();
  }
}

/**
 * Retrieves the timestamp for the current user's last view of a specified term.
 *
 * @param $tid
 *   A taxonomy term ID.
 *
 * @return
 *   If a taxonomy term has been previously viewed by the user, the timestamp in
 *   seconds of when the last view occurred; otherwise, zero.
 */
function taxonomy_term_last_viewed($tid) {
  global $user;
  $history = &drupal_static(__FUNCTION__, array());

  if (!isset($history[$tid])) {
    $history[$tid] = db_query("
      SELECT timestamp FROM {taxonomy_term_history}
      WHERE uid = :uid AND tid = :tid
    ", array(':uid' => $user->uid, ':tid' => $tid))->fetchObject();
  }

  return (isset($history[$tid]->timestamp) ? $history[$tid]->timestamp : 0);
}

/**
 * Determines the type of marker to be displayed for a given taxonomy term.
 *
 * @param $tid
 *   Taxonomy term ID whose history supplies the "last viewed" timestamp.
 * @param $timestamp
 *   Time which is compared against term's "last viewed" timestamp.
 *
 * @return
 *   One of the MARK constants.
 */
function taxonomy_term_changed_mark($tid, $timestamp) {
  global $user;
  $cache = &drupal_static(__FUNCTION__, array());

  if (!$user->uid) {
    return MARK_READ;
  }
  if (!isset($cache[$tid])) {
    $cache[$tid] = taxonomy_term_last_viewed($tid);
  }
  if ($cache[$tid] == 0 && $timestamp > TAXONOMY_TERM_NEW_LIMIT) {
    return MARK_NEW;
  }
  elseif ($timestamp > $cache[$tid] && $timestamp > TAXONOMY_TERM_NEW_LIMIT) {
    return MARK_UPDATED;
  }
  return MARK_READ;
}

/**
 * Finds the last time a taxonomy term was changed.
 *
 * @param $tid
 *   The ID of the taxonomy term.
 *
 * @return
 *   A Unix timestamp indicating the last time the taxonomy term was changed.
 */
function taxonomy_term_changed_last_changed($tid) {
  return db_query("
    SELECT changed FROM {taxonomy_term_data} WHERE tid = :tid
  ", array(":tid" => $tid))->fetch()->changed;
}

/**
 * Updates the last time a taxonomy term was changed.
 *
 * @param $tid
 *   The ID of the taxonomy term.
 */
function taxonomy_term_changed($tid) {
  db_query("
    UPDATE {taxonomy_term_data} SET changed = :timestamp WHERE tid = :tid
  ", array(
    ":timestamp" => REQUEST_TIME,
    ":tid" => $tid,
  ));
}

/**
 * Implements hook_taxonomy_term_presave().
 */
function taxonomy_term_changed_taxonomy_term_presave($term) {
  // The changed timestamp is always updated for bookkeeping purposes.
  $term->changed = REQUEST_TIME;
}

/**
 * Implements hook_taxonomy_term_update().
 */
function taxonomy_term_changed_taxonomy_term_update($term) {
  taxonomy_term_changed($term->tid);
}

/**
 * Implements hook_taxonomy_term_load().
 */
function taxonomy_term_changed_taxonomy_term_load($terms) {
  $results = db_query("
    SELECT tid, changed FROM {taxonomy_term_data} WHERE tid IN (:tids)
  ", array(
    ":tids" => array_keys($terms),
  ));
  foreach ($results as $record) {
    $terms[$record->tid]->changed = $record->changed;
  }
}

/**
 * Implements hook_taxonomy_term_view().
 */
function taxonomy_term_changed_taxonomy_term_view($term, $view_mode, $langcode) {
  // Update the history table, stating that this user viewed this taxonomy term.
  taxonomy_term_tag_new($term);
}

/**
 * Implements hook_taxonomy_term_delete().
 */
function taxonomy_term_changed_taxonomy_term_delete($term) {
  db_delete('taxonomy_term_history')
    ->condition('tid', $term->tid)
    ->execute();
}

/**
 * Implements hook_entity_property_info_alter().
 */
function taxonomy_term_changed_entity_property_info_alter(&$info) {
  $info['taxonomy_term']['properties']['changed'] = array(
    'label' => t('Changed'),
    'description' => t('The Unix timestamp when the term was most recently saved.'),
    'type' => 'date',
    'schema field' => 'changed',
  );
}

/**
 * Implements hook_user_delete().
 */
function taxonomy_term_changed_delete($account) {
  // Clean history.
  db_delete('taxonomy_term_history')
    ->condition('uid', $account->uid)
    ->execute();
}

/**
 * Implements hook_views_api().
 */
function taxonomy_term_changed_views_api() {
  return array(
    'api' => 3,
    'path' => drupal_get_path('module', 'taxonomy_term_changed') . '/views',
  );
}
