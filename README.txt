INTRODUCTION
------------

The Taxonomy term changed module adds the "changed" column to
{taxonomy_term_data} to allow tracking when taxonomy terms are updated, similar
to the "changed" column in the {node} table.


INSTALLATION
------------

Install as you would normally install a contributed Drupal module. See:
https://drupal.org/documentation/install/modules-themes/modules-7 for
further information.


MAINTAINERS
-----------

Current maintainers:
 * Jacob Embree (jacobembree) - https://www.drupal.org/u/jacobembree
