<?php

/**
 * @file
 * Views integration for Taxonomy term changed.
 */

/**
 * Implements hook_views_data().
 */
function taxonomy_term_changed_views_data() {
  $data['taxonomy_term_data']['changed'] = array(
    'title' => t('Changed'),
    'help' => t('The Unix timestamp when the term was most recently saved.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  $data['taxonomy_term_history_user']['moved to'] = 'taxonomy_term_history';
  $data['taxonomy_term_history']['table']['group'] = t('Taxonomy term');
  $data['taxonomy_term_history']['table']['join'] = array(
    'taxonomy_term_data' => array(
      'table' => 'taxonomy_term_history',
      'left_field' => 'tid',
      'field' => 'tid',
      'extra' => array(
        array('field' => 'uid', 'value' => '***CURRENT_USER***', 'numeric' => TRUE),
      ),
    ),
  );

  $data['taxonomy_term_history']['timestamp'] = array(
    'title' => t('Has new content'),
    'field' => array(
      'handler' => 'views_handler_field_taxonomy_term_history_user_timestamp',
      'help' => t('Show a marker if the term is new or updated.'),
    ),
    'filter' => array(
      'help' => t('Show only terms that are new or updated.'),
      'handler' => 'views_handler_filter_taxonomy_term_history_user_timestamp',
    ),
  );

  return $data;
}
