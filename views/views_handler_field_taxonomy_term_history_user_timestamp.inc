<?php

/**
 * @file
 * Definition of views_handler_field_taxonomy_term_history_user_timestamp.
 */

/**
 * Field handler to display the marker for new terms.
 *
 * @ingroup views_field_handlers
 */
class views_handler_field_taxonomy_term_history_user_timestamp extends views_handler_field_taxonomy {

  /**
   * {@inheritdoc}
   */
  public function init(&$view, &$options) {
    parent::init($view, $options);
    global $user;
    if ($user->uid) {
      $this->additional_fields['changed'] = array('table' => 'taxonomy_term_data', 'field' => 'changed');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Only add ourselves to the query if logged in.
    global $user;
    if (!$user->uid) {
      return;
    }
    parent::query();
  }

  /**
   * {@inheritdoc}
   */
  public function render($values) {
    // Let's default to 'read' state.
    // This code shadows node_mark, but it reads from the db directly and
    // we already have that info.
    $mark = MARK_READ;
    global $user;
    if ($user->uid) {
      $last_read = $this->get_value($values);
      $changed = $this->get_value($values, 'changed');

      if (!$last_read && $changed > TAXONOMY_TERM_NEW_LIMIT) {
        $mark = MARK_NEW;
      }
      elseif ($changed > $last_read && $changed > TAXONOMY_TERM_NEW_LIMIT) {
        $mark = MARK_UPDATED;
      }
      return $this->render_link(theme('mark', array('type' => $mark)), $values);
    }
  }

}
