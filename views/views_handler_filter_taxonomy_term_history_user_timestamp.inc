<?php

/**
 * @file
 * Definition of views_handler_filter_taxonomy_term_history_user_timestamp.
 */

/**
 * Filter for new terms.
 *
 * @ingroup views_filter_handlers
 */
class views_handler_filter_taxonomy_term_history_user_timestamp extends views_handler_filter {

  /**
   * Don't display empty space where the operator would be.
   */
  public $no_operator = TRUE;

  /**
   * {@inheritdoc}
   */
  public function expose_form(&$form, &$form_state) {
    parent::expose_form($form, $form_state);
    // @todo There are better ways of excluding required and multiple (object
    // flags)
    unset($form['expose']['required']);
    unset($form['expose']['multiple']);
    unset($form['expose']['remember']);
  }

  /**
   * {@inheritdoc}
   */
  public function value_form(&$form, &$form_state) {
    // Only present a checkbox for the exposed filter itself. There's no way
    // to tell the difference between not checked and the default value, so
    // specifying the default value via the views UI is meaningless.
    if (!empty($form_state['exposed'])) {
      if (isset($this->options['expose']['label'])) {
        $label = $this->options['expose']['label'];
      }
      else {
        $label = t('Has new terms');
      }
      $form['value'] = array(
        '#type' => 'checkbox',
        '#title' => $label,
        '#default_value' => $this->value,
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    global $user;
    // This can only work if we're logged in.
    if (!$user || !$user->uid) {
      return;
    }

    // Don't filter if we're exposed and the checkbox isn't selected.
    if ((!empty($this->options['exposed'])) && empty($this->value)) {
      return;
    }

    // Hey, Drupal kills old history, so terms that haven't been updated
    // since TAXONOMY_TERM_NEW_LIMIT are bzzzzzzzt outta here!
    $limit = REQUEST_TIME - TAXONOMY_TERM_NEW_LIMIT;

    $this->ensure_my_table();
    $field = "$this->table_alias.$this->real_field";
    $term = $this->query->ensure_table('taxonomy_term_data', $this->relationship);

    $clause = '';
    $clause2 = '';

    // NULL means a history record doesn't exist. That's clearly new terms.
    // Unless it's very very old terms. Everything in the query is already
    // type safe cause none of it is coming from outside here.
    $this->query->add_where_expression($this->options['group'], "($field IS NULL AND ($term.changed > (***CURRENT_TIME*** - $limit) $clause)) OR $field < $term.changed $clause2");
  }

  /**
   * {@inheritdoc}
   */
  public function admin_summary() {
    if (!empty($this->options['exposed'])) {
      return t('exposed');
    }
  }

}
